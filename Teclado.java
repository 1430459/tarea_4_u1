package com.clases;

import java.util.Objects;

public class Teclado {
    static int n =0;
    private int id;
    private String numeroDeSerie;
    private String marca;
    private String modelo;
    private String color;
    private String Numero_De_Parte;
    public Teclado(String a,String b, String c,String d,String e){
       this.numeroDeSerie = a;
       this.marca = b;
       this.modelo = c;
       this.color = d;
       this.Numero_De_Parte = e;
       this.id = n;
       n++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeroDeSerie() {
        return numeroDeSerie;
    }

    public void setNumeroDeSerie(String numeroDeSerie) {
        this.numeroDeSerie = numeroDeSerie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNumero_De_Parte() {
        return Numero_De_Parte;
    }

    public void setNumero_De_Parte(String Numero_De_Parte) {
        this.Numero_De_Parte = Numero_De_Parte;
    }
    
    @Override
    public String toString(){
       return this.id+","+this.numeroDeSerie+","+this.marca+","+
               this.modelo+","+this.color+","+this.Numero_De_Parte;
    }


    @Override
    public boolean equals(Object other){
         Teclado teclado = (Teclado)other;
         return teclado.getNumeroDeSerie().equals(this.numeroDeSerie) &&
         teclado.getModelo().equals(this.modelo) &&
                 teclado.getMarca().equals(this.marca);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.numeroDeSerie);
        hash = 29 * hash + Objects.hashCode(this.marca);
        hash = 29 * hash + Objects.hashCode(this.modelo);
        hash = 29 * hash + Objects.hashCode(this.color);
        hash = 29 * hash + Objects.hashCode(this.Numero_De_Parte);
        return hash;
    }
}